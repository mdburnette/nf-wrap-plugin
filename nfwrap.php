<?php
/*
Plugin Name: NF Wrap
Plugin URI: https://mburnette.com
Description: Wrap your Ninja Forms in a wrapper (like a fieldset).
Version: 1.1
Author: Marcus Burnette
Author URI: https://mburnette.com/
License: GPLv2 or later
Text Domain: nfwrap
*/


/**
 * HOW TO USE:
 * 1 - Within your Ninja Form, add an HTML field where you would like the wrap to begin.
 * 2 - Set the fieldset wrap title (<legend>) by entering the field label text of your choice.
 * 3 - Set the container class to "nfwrap-start" under the DISPLAY options.
 * 4 - Set the container class to "nfwrap-end" for the last field in the fieldset under the DISPLAY options.
 */


add_action('wp_enqueue_scripts', 'nfwrap_enqueue_scripts');
function nfwrap_enqueue_scripts() {
	wp_enqueue_script( 'nfwrap-js', plugin_dir_url( __FILE__ ) . 'js/nfwrap.js', array(jquery) );
}
