#INSTALLATION:

Upload plugin and enable. No settings required.


#HOW TO USE:

1. Within your Ninja Form, add an HTML field where you would like the wrap to begin.
2. Set the fieldset wrap title (<legend>) by entering the field label text of your choice.
3. Set the container class to "nfwrap-start" under the DISPLAY options.
4. Set the container class to "nfwrap-end" for the last field in the fieldset under the DISPLAY options.