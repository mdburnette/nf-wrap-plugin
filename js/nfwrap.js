
jQuery(function($) {
// -------------------------------------------------------------------

// ----- start when form is loaded
$(document).on('nfFormReady', function(){

	// check if any start fields have been set
	if( $('.nfwrap-start').length ){

		// determine if "Layout and Styles" plugin installed, update target fields
		let nfwrap_row_type = '';
		if( $('.nf-row').length ){
			nfwrap_row_type = '.nf-row';
		} else {
			nfwrap_row_type = 'nf-field';
		}

		// determine start and end fields - class appropriately
		$('.nfwrap-start').parents(nfwrap_row_type).addClass('nfwrap-start-field');
		$('.nfwrap-end').parents(nfwrap_row_type).addClass('nfwrap-end-field');

		// need to loop through to group multiple sets, start with set 1
		let nfwrap_set_loop_count = 1;

		// loop through sets of start/end fields and wrap
		$.each( $('.nfwrap-start-field'), function(){

			// determine legend text, hide original HTML field
			let nfwrap_legend = $(this).find('.nf-field-label').find('label').text().trim();
			$(this).hide();


			// add data label for nfwrap-start field for grouping
			$(this).attr('data', 'nfwrap-' + nfwrap_set_loop_count + '-field');

			// add data label for subsequent fields for grouping
			$(this).nextUntil('.nfwrap-end-field', nfwrap_row_type).attr('data', 'nfwrap-' + nfwrap_set_loop_count + '-field');

			// add data label for nfwrap-end field for grouping
			$(this).nextAll('.nfwrap-end-field', nfwrap_row_type).first().attr('data', 'nfwrap-' + nfwrap_set_loop_count + '-field');

			// group all fields with same data labels as above
			$('[data=nfwrap-' + nfwrap_set_loop_count + '-field]').wrapAll('<fieldset class="nfwrap-fieldset" id="nfwrap-' + nfwrap_set_loop_count + '" />');

			// add legend element/text if exists
			if( nfwrap_legend.length > 0 ){
				$('#nfwrap-' + nfwrap_set_loop_count).prepend('<legend>'+nfwrap_legend+'</legend>');
			}

			// increment loop count
			nfwrap_set_loop_count++;

		});

	}

});



// -------------------------------------------------------------------

});